<?php

namespace Drupal\entity_type_behaviors_anchor\Plugin\EntityTypeBehavior;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_type_behaviors\EntityTypeBehaviorBase;

/**
 * Class Example.
 *
 * @package Drupal\entity_type_behaviors\Plugin\EntityTypeBehavior
 *
 * @EntityTypeBehavior(
 *  id="anchor",
 *  description="This will expose a field that can be used as an anchor.",
 *  label=@Translation("Anchor behavior"),
 * )
 */
class Anchor extends EntityTypeBehaviorBase {

  /**
   * {@inheritdoc}
   */
  public function getForm(array $defaultValues = []): array {
    $element['anchor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Anchor'),
      "#description" => $this->t('You can use this as an in page reference using a fragment. e.g. www.example.com/some-page#fragment'),
      '#default_value' => $this->getValueByKey('anchor') ?? '',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function view(
    array &$build,
    EntityInterface $entity,
    EntityViewDisplayInterface $display,
    $view_mode
  ) {
    if (!$this->getValueByKey('anchor')) {
      return;
    }

    $build['#attributes']['id'] = Html::getId($this->getValueByKey('anchor'));
  }

}
