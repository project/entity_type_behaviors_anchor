# INTRODUCTION

A very simple implementation of entity type behaviors to add an anchor
to the output of an entity.

This will set the id of your content to what is defined in the behavior.

# REQUIREMENTS

The entity type behaviors module is required.

# INSTALLATION

- Install as you would normally install a contributed Drupal module. For further
  information, see _[Installing Drupal Modules]_.

[Installing Drupal Modules]: https://www.drupal.org/docs/extending-drupal/installing-drupal-modules

# CONFIGURATION

Follow instructions from the entity_type_behaviors module.
